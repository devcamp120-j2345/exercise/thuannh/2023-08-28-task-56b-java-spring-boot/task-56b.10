package com.devcamp.task56b_20.controller;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56b_20.models.Author;
import com.devcamp.task56b_20.models.Book;

@RestController
@RequestMapping("/")
@CrossOrigin
public class BookController {
    @GetMapping("/books")
    public ArrayList<Book> getBooks() {
        // Khởi tạo 3 đối tượng tác giả
        Author author1 = new Author("Author 1", "author1@gmail.com", 'm');
        Author author2 = new Author("Author 2", "author2@gmail.com", 'f');
        Author author3 = new Author("Author 3", "author3@gmail.com", 'm');

        // In các đối tượng tác giả ra console
        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);

        // Khởi tạo 3 đối tượng sách tương ứng với các tác giả
        Book book1 = new Book("Book 1", 10.0, 5, author1);
        Book book2 = new Book("Book 2", 20.0, 10, author2);
        Book book3 = new Book("Book 3", 30.0, 15, author3);

        // In các đối tượng sách ra console
        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);

        // Khởi tạo một ArrayList mới và thêm các đối tượng sách vào đó
        ArrayList<Book> books = new ArrayList<>(Arrays.asList(book1, book2, book3));

        return books;
    }
}
